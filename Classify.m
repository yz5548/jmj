clear;close;clc;
list=dir('data');
Nf=length(list);%number of files
%"data" refer to the directory store all data ready to process, need change
%according to real data diretory's name

type=nan(360,181,Nf-2,2);

for n=3:Nf;
filename=list(n).name;
load(['data\',filename]) %COD Press rain(kg/m2)

% classify rain type
type_rain=nan(size(rain)); %3mm/h=kg/m2
type_rain(rain<=2.5*3)            = 1;  % Drizzle:        mm/h [0,2.5]
type_rain(rain<=8*3 & rain>2.5*3) = 2;  % Moderate rain:  mm/h (2.5,8]
type_rain(rain<16*3 & rain>8*3)   = 3;  % Heavy rain:     mm/h (8,16)
type_rain(rain>=16*3)             = 4;  % Rainstorm:      mm/h [16,+oo)

%classify cloud type
type_cloud=nan(size(COD));
type_cloud(COD>0    & COD<3.6 & Press>680 & Press<=1000) =1; %??    COD [0,3.6) Press (600 1000]
type_cloud(COD>0    & COD<3.6 & Press>440 & Press<=680 ) =2;  %???   COD [0,3.6) Press(440 600]
type_cloud(COD>0    & COD<3.6 & Press>50  & Press<=440)  =3;   %??    COD [0,3.6) Press(50 440]
type_cloud(COD>=3.6 & COD<23  & Press>680 & Press<=1000) =4; %???  COD [3.6,23) (600 1000]
type_cloud(COD>=3.6 & COD<23  & Press>440 & Press<=680)  =5; %???  COD [3.6,23) Press(440 600]
type_cloud(COD>=3.6 & COD<23  & Press>50  & Press<=440)  =6; %???  COD [3.6,23) Press(50 440]
type_cloud(COD>=23  & COD<379 & Press>680 & Press<=1000) =7; %??    COD [23,379) (600 1000]
type_cloud(COD>=23  & COD<379 & Press>440 & Press<=680)  =8; %???  COD [23,379) Press(440 600]
type_cloud(COD>=23  & COD<379 & Press>50  & Press<=440)  =9; %???  COD [23,379) Press(50 440]

type(:,:,n-2,1)=type_rain;
type(:,:,n-2,2)=type_cloud;


end

%Probability of precip from different clouds=(rain_i&choud_j)/cloud_j
% i=1,2,3,4; j=1,2,3,...,9

Count_c=nan(360,181,9);     %cloud
Count_cp=nan(360,181,9,4);  %cloud & rain

Pij=nan(360,181,9,4);
for i=1:360;
    for j=1:181;
        C_cloud=zeros(9,1); Cij=zeros(9,4);
        for n=1:(Nf-2);
            if isnan(type(i,j,n,2))==0 %cloud
                C_cloud(type(i,j,n,2))=C_cloud(type(i,j,n,2))+1;
                if isnan(type(i,j,n,1))==0
                    Cij(type(i,j,n,2),type(i,j,n,1))=Cij(type(i,j,n,2),type(i,j,n,1))+1;
                end
            end
        end
        Count_c(i,j,:)    = C_cloud;
        Count_cp(i,j,:,:) = Cij;
        for id=1:9;
            Pij(i,j,id,:)=Cij(id,:)./C_cloud(id);
        end

            
        
    end
end


%devide data China & US 
%?China?lon 70.125-139.875   lat?15.125-59.875?***index lon[70-140] Lat[15 60]-> [30 75]+1->[31 75]
%?US?lon?230-286?lat?23-54?  90~(-90) ***index lon[230 286] lat[23 54]->[36 76]+1->[37 77]

CN_c  = Count_c(70:140, 31:75,:);%China region
CN_cp = Count_cp(70:140, 31:75,:,:);

US_c  = Count_c(230:286,37:77,:);%US region
US_cp = Count_cp(230:286,37:77,:,:);


%CN_P=nan(size(CN_cp));US_P=nan(size(US_cp));
CN_P=nan(9,4);US_P=nan(9,4);
 for cp=1:9;
     for r=1:4; %rain
         CN_P(cp,r)=nansum(nansum(CN_cp(:,:,cp,r)))/nansum(nansum(CN_c(:,:,cp)));
         US_P(cp,r)=nansum(nansum(US_cp(:,:,cp,r)))/nansum(nansum(US_c(:,:,cp)));
     end
 end
save('Probability','CN_P','US_P','Pij')




